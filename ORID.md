## O:

Learned front-end and back-end coordination and did presentation today

## R:

No big problem.

## I:

- ***front-end and back-end coordination***：Today, I used springboot to build a back-end interface and connect to a mysql database to create real data instead of Mock simulated data, and by the way, I reviewed interface integration testing.
- ***responsive layout***:Through CodeReview, I learnt to set the layout under different resolutions through @media, and I also learnt to use the Grid layout in AntD component library to achieve adaptive layout.
- ***presentation***:Today's presentation let me know what is a lift speech and MVP, User Story and User JourneyMap.These are the knowledge of requirement analysis, understanding these can also make a better product welcomed by the public

## D:

Today's front-end and back-end coordination of the knowledge of these weeks are linked together, I rarely write JAVA and JAVA back-end, the knowledge of the test is a blank, through the learning in the ITA so that I know the TDD, and through the practice of the class and after-school homework so that my level of JAVA programming to improve a lot of people, but also the use of springboot to complete the basic add, delete, change, and check the interface function

