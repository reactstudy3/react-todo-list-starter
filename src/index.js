import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';
import HelpPage from './pages/HelpPage';
import DoneList from './features/todo/components/DoneList'
import store from './store'
import { Provider } from 'react-redux'
import { createBrowserRouter,RouterProvider } from 'react-router-dom';
import ErrorPage from './pages/ErrorPage';
import TodoList from './features/todo/components/TodoList';
import DoneDetail from './features/todo/components/DoneDetail';


const root = ReactDOM.createRoot(document.getElementById('root'));

const router = createBrowserRouter([
  {
    path:"/",
    element:<App/>,
    errorElement:<ErrorPage/>,
    children: [
      {
        index:true,
        element:<TodoList/>
      },
      {
        path:'/help',
        element:<HelpPage/>
      },
      {
        path:'/done',
        element:<DoneList/>
      },
      {
        path:'/done/:id',
        element:<DoneDetail/>
      }
    ]
  },
 
])

root.render(
  <React.StrictMode>
    <Provider store={store}>   
      <RouterProvider router={router}/>
    </Provider>
  </React.StrictMode>
);
