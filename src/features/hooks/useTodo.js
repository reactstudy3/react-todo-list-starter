import { useDispatch } from 'react-redux'
import * as todoService from '../../api/todo'
import { initTodoTask } from '../todo/reducers/todoSlice'

export const useTodos = ()=>{
    const dispatch = useDispatch()

    const loadTodos = async()=>{
        const res = await todoService.getTodoTasks()
        dispatch(initTodoTask(res.data))
    }

    const updateTodo = async(id,todoTask)=>{
        await todoService.updateTodoTask(id,todoTask)
        await loadTodos()
    }

    const deleteTodo = async(id)=>{
        await todoService.deleteTodoTask(id)
        await loadTodos()
    }
    const createTodo = async(todoTask)=>{
        await todoService.createTodoTask(todoTask)
        await loadTodos()
    }


    return {loadTodos,updateTodo,deleteTodo,createTodo}
}