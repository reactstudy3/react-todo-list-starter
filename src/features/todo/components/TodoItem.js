import { useTodos } from "../../hooks/useTodo";
import { Button, Modal, Form, Input, message, Popover } from "antd";
import { useState } from "react";

import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleFilled,
  EyeOutlined
} from "@ant-design/icons";

export default function TodoItem({ task }) {
  const { updateTodo, deleteTodo } = useTodos();
  const handleTaskNameClick = async () => {
    await updateTodo(task.id, { done: !task.done });
  };

  const { confirm } = Modal;
  const showDeleteConfirm = () => {
    confirm({
      title: "Are you sure delete this task?",
      icon: <ExclamationCircleFilled />,
      content: "💔Please check again.",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        deleteTodo(task.id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const onFinish = async (values) => {
    await updateTodo(task.id, { name: values.editTask });
    messageApi.open({
      type: "success",
      content: "Edit Success! Hope you're doing well with the task list",
      className: "custom-class",
      style: {
        marginTop: "10vh",
      },
    });
    setIsModalOpen(false);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const initialValues = {
    input: "",
  };

  const [messageApi, contextHolder] = message.useMessage();

  const content = (
    <div>
      <h3>ID: {task.id}</h3>
      <h3>Name: {task.name}</h3>
    </div>
  );
  return (
    <div className="todo-item">
      {contextHolder}
      <div
        className={`task-name ${task.done ? "done" : ""}`}
        onClick={handleTaskNameClick}
      >
        {task.name}
      </div>
      <div className="ButtonGroup">
        <div className="checkButton">
          <Popover content={content} title="Task Detail" trigger="hover">
            <Button><EyeOutlined /></Button>
          </Popover>
        </div>
        <div className="editButton">
          <Button type="primary" onClick={showModal}>
            <FormOutlined />
          </Button>
          <Modal
            title="Edit Todo Item 💗"
            open={isModalOpen}
            onCancel={handleCancel}
            footer={[]}
          >
            <Form
              className="formEdit"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              initialValues={initialValues}
              autoComplete="off"
            >
              <Form.Item label="To do" name="editTask">
                <Input name="input" />
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  OK
                </Button>
                <Button onClick={handleCancel}>Cancel</Button>
              </Form.Item>
            </Form>
          </Modal>
        </div>
        <Button
          className="remove-button"
          onClick={showDeleteConfirm}
          type="primary"
          danger
        >
          <DeleteOutlined />
        </Button>
      </div>
    </div>
  );
}
