import { Card } from "antd";
import DoneGroup from "./DoneGroup";

export default function TodoList() {
  return (
    <div className="todo-list">
      <Card title="Done List" className="card" >
        <DoneGroup />
      </Card>
    </div>
  );
}
