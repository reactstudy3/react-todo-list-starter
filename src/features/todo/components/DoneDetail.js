import { useSelector } from "react-redux"
import { useParams } from "react-router-dom"

export default function DoneDetail(){
    const {id} = useParams()
    const todoTask = useSelector(state => state.todo.tasks).find(task =>task.id === id)

    return (
        <div className="todoDetail">
            <h1>Done Detail</h1>
            <h3>ID: {todoTask.id}</h3>
            <h3>Name: {todoTask.name}</h3>
        </div>
    )
}
