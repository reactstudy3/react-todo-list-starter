import DoneItem from "./DoneItem";
import { useSelector } from 'react-redux'

export default function TodoGroup() {
    const donetodoTasks = useSelector(state => state.todo.tasks).filter(task =>task.done)
    return (
        <div className='todo-group'>
            {donetodoTasks.map(((todoTask) =>
                <DoneItem key= {todoTask.id} task={todoTask}></DoneItem>
            ))}
        </div>
    );
}