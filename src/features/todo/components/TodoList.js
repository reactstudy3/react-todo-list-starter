import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";

import { useEffect } from "react";
import { useTodos } from "../../hooks/useTodo";
import { Card } from "antd";

export default function TodoList() {
  const { loadTodos } = useTodos();
  useEffect(() => {
    loadTodos();
  });

  return (
    <div className="todo-list">
      <Card title="Todo List" className="card">
        <TodoGroup />
        <TodoGenerator />
      </Card>
      {/* <h1>Todo List</h1> */}
    </div>
  );
}
