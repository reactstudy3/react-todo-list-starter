import { useNavigate } from "react-router-dom";
import { useTodos } from "../../hooks/useTodo";
import { Button,  message } from "antd";
import { DeleteOutlined} from "@ant-design/icons";

export default function DoneItem({ task }) {
  const navigate = useNavigate();
  const { deleteTodo } = useTodos();
  const handleTaskNameClick = () => {
    navigate("/done/" + task.id);
  };

  const hanlleRemoveButtonClick = async () => {
    if (window.confirm("Are you sure you wish to delete this item?")) {
      await deleteTodo(task.id);
    }
  };

  return (
    <div className="todo-item">
      <div
        className={`task-name ${task.done ? "done" : ""}`}
        onClick={handleTaskNameClick}
      >
        {task.name}
      </div>
      <Button className="remove-button" onClick={hanlleRemoveButtonClick} type="primary" danger>
      <DeleteOutlined />
      </Button>
    </div>
  );
}
