import api from './api'

export const getTodoTasks = () => {
    return api.get('/tasks')
}
export const updateTodoTask = (id,todoTask) =>{
    return api.put(`tasks/${id}`,todoTask)
}
export const deleteTodoTask = (id) => {
    return api.delete(`tasks/${id}`)
}
export const createTodoTask = (todoTask)=>{
    return api.post(`tasks`,todoTask)
}