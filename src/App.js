import "./App.css";
import { Link, Outlet } from "react-router-dom";
import { Menu } from 'antd';
import { AppstoreOutlined, MailOutlined,MenuUnfoldOutlined} from '@ant-design/icons'
import React, { useState } from 'react';

function App() {
  const [current, setCurrent] = useState('mail');
  const onClick = (e) => {
    console.log('click ', e);
    setCurrent(e.key);
  };
  const items = [
    {
      label: (<Link to="/">Home</Link>),
      key: 'Home',
      icon: <MailOutlined />,
    },
    {
      label: (<Link to="/done">Done List</Link>),
      key: 'Done',
      icon: <AppstoreOutlined />,
     
    },
    {
      label: (<Link to="/help">Help</Link>),
      key: 'Help',
      icon: <MenuUnfoldOutlined />,
      
    },
  
  ]


  return (
    <div className="app">
      <div className="nav-bar">
        <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items}></Menu>
      </div>
      <Outlet/>
    </div>
  );
}
export default App;
